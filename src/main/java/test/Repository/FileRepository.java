package test.Repository;

import test.Model.DBFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<DBFile,String> {
}
