package test.Model;

import lombok.Data;
import lombok.experimental.Accessors;
import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

//ssoid;ts;grp;type;subtype;url;orgid;formid;code;ltpa;sudirresponse;ymdh

@Entity
@Table(name = "DBFile")
@Data
@Accessors(chain = true)
public class DBFile {
    @Id
    @Column(name = "ssoid")
    String ssoid;

    @Column(name = "ts")
    BigInteger ts;

    @Column(name = "grp")
    String grp;

    @Column(name = "type")
    String type;

    @Column(name = "subtype")
    String subtype;

    @Column(name = "url")
    String url;

    @Column(name = "orgid")
    String orgid;

    @Column(name = "formid")
    String formid;

    @Column(name = "code")
    String code;

    @Column(name = "ltpa")
    BigInteger ltpa;

    @Column(name = "sudirresponse")
    String sudirresponse;

    @Column(name = "ymdh")
    Date ymdh; //YYYY-MM-DD-HH
}