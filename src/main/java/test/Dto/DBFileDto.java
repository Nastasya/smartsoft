package test.Dto;

import java.util.Date;

public class DBFileDto {
    String ssoid;
    Long ts;
    String grp;
    String type;
    String subtype;
    String url;
    String orgid;
    String formid;
    Long ltpa;
    String sudirresponse;
    Date ymdh;
    String code;
}
